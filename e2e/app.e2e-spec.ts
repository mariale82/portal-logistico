import { PortalLogisticoMilanesPage } from './app.po';

describe('portal-logistico-milanes App', function() {
  let page: PortalLogisticoMilanesPage;

  beforeEach(() => {
    page = new PortalLogisticoMilanesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
