import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './feature-areas/main/main.component';


import { AsignarMensajeroComponent } from './feature-areas/asignar-mensajero/asignar-mensajero.component';
import { HomeComponent } from './feature-areas/ingreso-planta/home/home.component';
import { IngresoClienteComponent } from './feature-areas/ingreso-planta/ingreso-cliente/ingreso-cliente.component';
import { IngresoTransportadoraComponent } from './feature-areas/ingreso-planta/ingreso-transportadora/ingreso-transportadora.component';

import { AsignarMensajero2Component } from './feature-areas/asignar-mensajero/asignar-mensajero2/asignar-mensajero2.component';

import { DetalleRegistroComponent } from './feature-areas/ingreso-planta/components/detalle-registro/detalle-registro.component';

import { RecogidaClienteComponent } from './feature-areas/main/components/recogida-cliente/recogida-cliente.component';
//import { RecogidaTransportadoraComponent } from './feature-areas/main/components/recogida-transportadora/recogida-transportadora.component';


export const routes: Routes = [
    {
        path: '',
        redirectTo: 'main',
        pathMatch: 'full' 

    }
];

export const APP_ROUTER_PROVIDERS: Array<{}> = [

];

export const routing = RouterModule.forRoot(routes, { useHash: true });
