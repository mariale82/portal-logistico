import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
//import { MainComponent } from './feature-areas/main/main.component';

import {
  MegaMenuModule,
  DataTableModule,
  SharedModule,
  DropdownModule
} from 'primeng/primeng';

//lookinto this
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { routing, APP_ROUTER_PROVIDERS } from './app-routing.module';

import { NgSemanticModule } from 'ng-semantic/ng-semantic';

//Esto no deberia ir aqui sino en el modulo de Asignar mensajero
import { FieldsetModule } from 'primeng/primeng';
import { MultiSelectModule } from 'primeng/primeng';
//

//Esto no deberia ir aqui sino en el modulo de Ingreso a Planta
import { CalendarModule } from 'primeng/primeng';
//

//Esto no deberia estar aqui sino en Ingreso Planta transportadora

import { TabViewModule } from 'primeng/primeng';

import { IngresoClienteComponent } from './feature-areas/ingreso-planta/ingreso-cliente/ingreso-cliente.component';
import { IngresoTransportadoraComponent } from './feature-areas/ingreso-planta/ingreso-transportadora/ingreso-transportadora.component';
import { HomeComponent } from './feature-areas/ingreso-planta/home/home.component';

import { IngresoPlantaModule } from './feature-areas/ingreso-planta/ingreso-planta.module';

import { MainModule } from './feature-areas/main/main.module';

@NgModule({
  declarations: [
    AppComponent,
    IngresoClienteComponent,
    IngresoTransportadoraComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    MegaMenuModule,
    NgSemanticModule,
    DataTableModule,
    SharedModule,
    FieldsetModule,
    CalendarModule,
    IngresoPlantaModule,
    ReactiveFormsModule,
    TabViewModule,
    MultiSelectModule,
    MainModule,
    DropdownModule
    //
  ],
  providers: [APP_ROUTER_PROVIDERS],
  bootstrap: [AppComponent]
})
export class AppModule { }
