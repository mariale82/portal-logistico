import { Component, OnInit } from '@angular/core';
import { IngresoClienteService } from '../services/ingreso-cliente.service';
import { PendingModel } from '../../../model/ingreso-planta/PendientesIngreso';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ingreso-cliente',
  templateUrl: './ingreso-cliente.component.html',
  styleUrls: ['./ingreso-cliente.component.css'],
  providers: [IngresoClienteService]
})
export class IngresoClienteComponent implements OnInit {

  private emptyMessage = 'No hay registros de nuevos ingresos';
  pendingTasks: PendingModel[];
  private receivedDate: Date;
  private items = {
    received: false,
    receivedDate: ''
  };
  private selectedRow = [];

  constructor(private _ingresoClienteSrvc: IngresoClienteService, private router: Router) { }

  ngOnInit() {
    this.getPendingTasks();
  }

  getPendingTasks() {
    this._ingresoClienteSrvc.getPending()
      .then(pt => this.pendingTasks = pt);
  }

  packageReceived(rcvd: PendingModel) {
    this.receivedDate = new Date();   
    console.log("entra por parametros => " + JSON.stringify(rcvd));
    if (rcvd.receivedDate==null) rcvd.receivedDate = this.receivedDate;
    else rcvd.receivedDate = null;

    // for (var i = 0; i <= this.selectedRow.length - 1; i++) {
    //   this.pendingTasks[i].receivedDate = this.receivedDate;
    // }

    

  }

  selected(coll) {
    console.log(coll);
     let link = ['/ingresoCliente/detalle'];
        this.router.navigate(link);

  }
  // selected(pt: PendingModel) {
  //   let $index = this.tasks.indexOf(pt.client);
  //   if ($index < 0) this.tasks.unshift(pt.client);
  //   else this.tasks.splice($index, 1);
  // }

  // packageReceived(item) {
  //   console.log(item);
  //   console.log("paquete recibido " + item + " dia " + this.receivedDate);

  // }

}
