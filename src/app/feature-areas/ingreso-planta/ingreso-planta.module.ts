import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MegaMenuModule } from 'primeng/primeng';
import { MenuModule, MenuItem } from 'primeng/primeng';
import { CalendarModule, FieldsetModule } from 'primeng/primeng';

//import { AsignarMensajeroModule } from '../asignar-mensajero/asignar-mensajero.module';

import { NgSemanticModule } from 'ng-semantic';
import { DetalleRegistroComponent } from './components/detalle-registro/detalle-registro.component';


@NgModule({
  declarations: [DetalleRegistroComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    MegaMenuModule,
    MenuModule,
    FieldsetModule,
    CalendarModule
  ],
  providers: []
})
export class IngresoPlantaModule { }
