import { Component, OnInit } from '@angular/core';
import { IngresoTransportadoraService } from '../services/ingreso-transportadora.service';
import { WorkRequestPipe } from '../pipes/ingreso-planta.pipe';

@Component({
  selector: 'app-ingreso-transportadora',
  templateUrl: './ingreso-transportadora.component.html',
  styleUrls: ['./ingreso-transportadora.component.css'],
  providers: [IngresoTransportadoraService]
})
export class IngresoTransportadoraComponent implements OnInit {

  private ingresoTransportadoraForm;
  private ingresoTransportadora;
  private receivedDate: Date = new Date();
  errorMessage: string;
  private pipe: WorkRequestPipe = new WorkRequestPipe();

  constructor(private _ingresoPlantaService: IngresoTransportadoraService) {
    this.ingresoTransportadoraForm = _ingresoPlantaService.form;
    this.ingresoTransportadora = _ingresoPlantaService.ingresoTransportadora;
  }

  ngOnInit() {
  }

  register() {
    console.log(this.ingresoTransportadoraForm.value);
    let work = this.pipe.encode(this.ingresoTransportadora);
    this._ingresoPlantaService
      .createCollectRequest(this.ingresoTransportadora)
      .subscribe(
         p => console.log(p),
         e => this.errorMessage = e);
  }

}
