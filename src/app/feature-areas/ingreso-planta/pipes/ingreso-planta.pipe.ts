import { Pipe, PipeTransform } from '@angular/core';
import { WorkRequest as WorkBack } from '../../../model/work-request/work-request';
import { WorkRequest as WorkFront } from '../../../model/solicitud-recogida/work-request';

@Pipe({
  name: 'workRequest'
})
export class WorkRequestPipe implements PipeTransform {

  encode(workRequest: WorkFront): WorkBack {
    let parameters = [];
    let jobOrder = {
      description: 'Ingreso a Planta',
      location: {id:1},
      workType: 'PRODUCTION',
      workMaster: 'portalLogistico',
      startTime: new Date(),
      endTime: null,
      tenantId: 1,
      parameters: [],
      materialRequirements: []
    };

    let copyRequest = Object.assign({}, workRequest);
    delete copyRequest.id;
    delete copyRequest.description;
    delete copyRequest.customer;

    for (let code in copyRequest) {
      jobOrder.parameters.push({
        code: code,
        value: copyRequest[code],
        parameters: []
      })
    }
    return {
      location: {id:1},
      tenantId: 1,
      priority: 'HIGH',
      jobOrders: [jobOrder],
      workRequests: []
    };
    
  }

  decode(workRequest: WorkBack): WorkFront {
    return null;
  }

  transform(value: WorkBack): WorkFront {
    return this.decode(value);
  }

}
