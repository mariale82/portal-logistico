import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {PendingModel} from '../../../model/ingreso-planta/PendientesIngreso'; 

@Injectable()
export class IngresoClienteService {

    private formBuilder: FormBuilder;
    public form: FormGroup;

    private url = 'http://localhost:8081';
    
    constructor(private http: Http) {
    
    }

    getPending() {
        console.log("entre");
        return this.http.get('app/feature-areas/ingreso-planta/services/listadePendientesPorIngresar.json')
            .toPromise()
            .then(res => <PendingModel[]>res.json().data)
            .then(data => { return data; });
    }


    private extractData(res: Response) {
        let body = res.json();
        console.log("extractedData => " + JSON.stringify(body));
        return body.data || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}