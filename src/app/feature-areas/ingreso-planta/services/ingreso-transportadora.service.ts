import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { WorkRequest } from '../../../model/ingreso-planta/work-request';

@Injectable()
export class IngresoTransportadoraService {

    private apiUrl = 'http://localhost:8081/runtime/process-instances';

    private formBuilder: FormBuilder;
    public form: FormGroup;
    public ingresoTransportadora: WorkRequest = {
        parentId: null,
        workType: '',
        status: 'Activo',
        priority: '',
        processingType: '',
        description: '',
        courier: '',
        clientName: '',
        guideNumber: '',
        receivedDate: ''
    };


    constructor(fb: FormBuilder, private http: Http) {
        this.formBuilder = fb;
        this.buildForm();
    }

    buildForm(): void {
        this.form = this.formBuilder.group({
            clientName: [this.ingresoTransportadora.clientName],
            courier: [this.ingresoTransportadora.courier, Validators.required],
            guideNumber: [this.ingresoTransportadora.guideNumber, Validators.required],
            receivedDate: [this.ingresoTransportadora.receivedDate, Validators.required]
        })
    }


    private extractData(res: Response) {
        let body = res.json();
        //console.log("extractedData => " + JSON.stringify(body));
        return body.data || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    createCollectRequest(wr: WorkRequest): Observable<Response> {
        let data = this.entryData();
        //console.log("DATA => "+data);
        let headers = this.getHeaders();

        let resultado$ = this.http
            .post(`${this.apiUrl}`, data, { headers: headers })
            .map((res: Response) => res.json())
            .catch(this.handleError);
        return resultado$;
        //console.log("JSON savePost =>" + JSON.stringify(wr));
    }

    private entryData() {
        let data = `
        {
            "processDefinitionId":"portalLogistico:1:11",
            "businessKey":"portalLogistico",
            "variables": [{
                "name":"es_recogida",
                "value":"false"
            },
            {
                "name":"messenger",
                "value":"maria"
            }
            ]
        }`;
        return data;
    }

    private getHeaders() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Accept', 'application/json');
        headers.append('Authorization', 'Basic ' + btoa('admin:admin'));
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, x-auth-token, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        headers.append('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
        headers.append('Access-Control-Allow-Credentials', "true");
        headers.append('X-Requested-With', 'XMLHttpRequest');
        return headers;
    }
}
