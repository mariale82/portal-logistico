/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AsignarMensajero2Component } from './asignar-mensajero2.component';

describe('AsignarMensajero2Component', () => {
  let component: AsignarMensajero2Component;
  let fixture: ComponentFixture<AsignarMensajero2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarMensajero2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarMensajero2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
