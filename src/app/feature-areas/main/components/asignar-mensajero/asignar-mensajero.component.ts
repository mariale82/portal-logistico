import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';

import { AsignacionMensajeroService } from '../../services/asignar-mensajero.service';
import { PendingModel } from '../../../../model/asignar-mensajero/PendientesAsignacion';
import { MessengersModel } from '../../../../model/asignar-mensajero/messengers';

@Component({
  selector: 'app-asignar-mensajero',
  templateUrl: './asignar-mensajero.component.html',
  styleUrls: ['./asignar-mensajero.component.css'],
  providers: [AsignacionMensajeroService]
})
export class AsignarMensajeroComponent implements OnInit {

  // items: SelectItem[];
  selectedItems: string[];
  pendingTasks: PendingModel[];
  messengers: MessengersModel[] = [];
  errorMessage: string;
  isLoading: boolean = true;
  private emptyMessage = 'No hay solicitudes de recogida a asignar';
  private ptasks = {
    messenger: ''
  };
  private tasks = [];
  //selectedTasks: PendingModel;
  private selectedTasks = [];


  private tmppending: PendingModel[] = [
    { "rdate": "20/01/2016", "client": "MORALES MARIA", "boffice": "", "courier": "", "messenger": "", "receivedDate": ""},
    {"rdate": "20/01/2016", "client": "NAVA FIORELLA", "boffice": "BOGOTA", "courier": "Transarmenia", "messenger": ""}
  ];

  private tmpassigned: PendingModel[] = [

  ];

  constructor(private _asigMensajeroSrvc: AsignacionMensajeroService) {

  }

  ngOnInit() {
    this.getMessengersList();
    this.getPendingTasks();
  }

  getMessengersList() {
    this._asigMensajeroSrvc
      .getMessengersList()
      .subscribe(
      messengers => this.messengers = messengers,
      () => this.isLoading = false
      );
    console.log('success =>' + JSON.stringify(this.messengers))
  }


  getPendingTasks() {

    this._asigMensajeroSrvc.getPendingTask()
      .subscribe(pendingTasks => this.pendingTasks = pendingTasks);
    console.log('success =>' + JSON.stringify(this.pendingTasks))

    /*this._asigMensajeroSrvc.getTask()
      .subscribe(pendingTasks => this.pendingTasks = pendingTasks);
      */
  }

  add() {
    if (this.ptasks.messenger && this.selectedTasks.length > 0) {
      for (var i = 0; i <= this.selectedTasks.length - 1; i++) {
        console.log("longitud "+this.selectedTasks.length);
        this.selectedTasks[i].messenger = this.ptasks.messenger;
        this.tmppending.splice(i,1);
        let $index = this.tmpassigned.indexOf(this.selectedTasks[i]);
        if($index < 0) this.tmpassigned.push(this.selectedTasks[i]);
      }
    }
  }

  // selected(pt: PendingModel) {
  //   let $index = this.tasks.indexOf(pt.client);
  //   if ($index < 0) this.tasks.unshift(pt.client);
  //   else this.tasks.splice($index, 1);
  // }

}
