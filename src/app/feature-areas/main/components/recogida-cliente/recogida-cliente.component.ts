import { Component, OnInit } from '@angular/core';
import { SolicitudRecogidaService } from '../../services/solicitud-recogida.service';
import { WorkRequestPipe } from '../../pipes/recogida-cliente.pipe';

@Component({
  selector: 'app-recogida-cliente',
  templateUrl: './recogida-cliente.component.html',
  styleUrls: ['./recogida-cliente.component.css'],
  providers: [SolicitudRecogidaService]
})
export class RecogidaClienteComponent implements OnInit {

  client: any;
  clients: any[];
  addresses: any[];
  filteredClientSingle: any[];
  errorMessage: string;
  private recogidaClienteForm;
  private recogidaCliente;
  private pipe: WorkRequestPipe = new WorkRequestPipe();
  quantity: number = 1;
  minDate: Date;


  constructor(private _RecogidaService: SolicitudRecogidaService) {
    this.recogidaClienteForm = _RecogidaService.form;
    this.recogidaCliente = _RecogidaService.recogidaCliente;
  }

  ngOnInit() {
    let today = new Date();
    this.minDate = new Date(today);
  }

  filterClientSingle(event) {
    let query = event.query;
    this._RecogidaService.getCustomers().then(
      clients => { this.filteredClientSingle = this.filterClient(query, clients); },
      error => this.errorMessage = <any>error);
  }

  onChange(item: any) {
    console.log("Direcciones del cliente")
    this.addresses = item.address;
  }

  filterClient(query, clients: any[]): any[] {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = [];
    for (let i = 0; i < clients.length; i++) {
      let client = clients[i];
      if (client.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(client);
      }
    }
    console.log(filtered);
    return filtered;
  }

  generate() {
    //console.log("entrando a generar solicitud");
    console.log(this.recogidaClienteForm.value);
    let work = this.pipe.encode(this.recogidaCliente);
    console.log(JSON.stringify(this.recogidaCliente));
    console.log(JSON.stringify(work));
    //this._RecogidaService.createCollectRequest(this.recogidaCliente);
    this._RecogidaService
      .createCollectRequest(this.recogidaCliente)
      .subscribe(
         /* happy path */ p => console.log(p),
         /* error path */ e => this.errorMessage = e);
  }

}
