import { Component, OnInit } from '@angular/core';
import { SolicitudRecogidaTransportadoraService } from '../../services/solicitud-recogida.transportadora.service';
import { WorkRequestTransportadoraPipe } from '../../pipes/recogida-transportadora.pipe';

@Component({
  selector: 'app-recogida-transportadora',
  templateUrl: './recogida-transportadora.component.html',
  styleUrls: ['./recogida-transportadora.component.css'],
  providers: [SolicitudRecogidaTransportadoraService]
})
export class RecogidaTransportadoraComponent implements OnInit {

  private recogidaTransportadoraForm;
  private recogidaTransportadora;
  private pipe: WorkRequestTransportadoraPipe = new WorkRequestTransportadoraPipe();
  maxDate: Date;
  minDate: Date;
  errorMessage: string;
  estimatedDate: Date;

  constructor(private _RecogidaService: SolicitudRecogidaTransportadoraService) {
    this.recogidaTransportadoraForm = _RecogidaService.form;
    this.recogidaTransportadora = _RecogidaService.recogidaTransportadora;
  }

  ngOnInit() {
    let today = new Date();
    this.maxDate = new Date(today);
  }

  dateSelected(date) {
    date = new Date(date);
    this.estimatedDate = new Date(date);
    this.estimatedDate.setDate(this.estimatedDate.getDate() + 1);
    console.log(this.estimatedDate);
    this.minDate = this.estimatedDate;
  }

  generate() {
    console.log("entrando a generar solicitud");
    console.log(this.recogidaTransportadoraForm.value);
    let work = this.pipe.encode(this.recogidaTransportadora);
    console.log(JSON.stringify(this.recogidaTransportadora));
    console.log(JSON.stringify(work));
    this._RecogidaService
      .createCollectRequest(this.recogidaTransportadora)
      .subscribe(
         /* happy path */ p => console.log(p),
         /* error path */ e => this.errorMessage = e);
  }

}
