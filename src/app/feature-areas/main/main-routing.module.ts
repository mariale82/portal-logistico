import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//look into this
//import { WorkRequestFormResolver } from './services/work-request-form.resolver';
import { Observable } from 'rxjs/Rx';
import { MainComponent } from './main.component';
import { RecogidaClienteComponent } from './components/recogida-cliente/recogida-cliente.component';
import { RecogidaTransportadoraComponent } from './components/recogida-transportadora/recogida-transportadora.component';
import { AsignarMensajeroComponent } from './components/asignar-mensajero/asignar-mensajero.component';

export const MainModuleRoutes: Routes = [
    {
        path: '',
        component: MainComponent,
        children: [
            { path: 'recogidaCliente', component: RecogidaClienteComponent },
            { path: 'recogidaTransportadora', component: RecogidaTransportadoraComponent },
            { path: 'asignarMensajero', component: AsignarMensajeroComponent}
        ]
    }

];

/* @NgModule({
    imports: [
        RouterModule.forChild(MainModuleRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class MainRoutingModule { }

*/
export const routing = RouterModule.forRoot(MainModuleRoutes, { useHash: true });