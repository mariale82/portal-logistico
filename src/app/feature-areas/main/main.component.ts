import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  private iplanta = [];
  private srecogida = [];

  constructor() { }

  ngOnInit() {
    this.getMenuItemsSemantic();
  }

  getMenuItemsSemantic() {
    this.iplanta = [
      {
        title: 'Ingreso Cliente',
        link: '#/ingresoCliente',
      },
      {
        title: 'Ingreso Transportadora',
        link: '#/ingresoTransportadora',
      }
    ];
    this.srecogida = [
      {
        title: 'Recogida Cliente',
        link: '#/recogidaCliente',
      },
      {
        title: 'Recogida Transportadora',
        link: '#/recogidaTransportadora',
      }
    ];
  }

}
