import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
  MegaMenuModule,
  MenuModule,
  MenuItem,
  AutoCompleteModule,
  FieldsetModule,
  CalendarModule,
  DropdownModule,
  SpinnerModule,
  InputTextareaModule,
  DataTableModule,
  SharedModule
} from 'primeng/primeng';
import { MainComponent } from './main.component';
import { NgSemanticModule } from 'ng-semantic/ng-semantic';
import { routing } from './main-routing.module';

import { RecogidaClienteComponent } from './components/recogida-cliente/recogida-cliente.component';
import { RecogidaTransportadoraComponent } from './components/recogida-transportadora/recogida-transportadora.component';
import { AsignarMensajeroComponent } from './components/asignar-mensajero/asignar-mensajero.component';

@NgModule({
  declarations: [
    MainComponent,
    RecogidaClienteComponent,
    RecogidaTransportadoraComponent,
    AsignarMensajeroComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    MegaMenuModule,
    MenuModule,
    AutoCompleteModule,
    FieldsetModule,
    CalendarModule,
    DropdownModule,
    NgSemanticModule,
    routing,
    SpinnerModule,
    InputTextareaModule,
    DataTableModule,
    SharedModule
  ],
  providers: []
})
export class MainModule { }
