import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { PendingModel } from '../../../model/asignar-mensajero/PendientesAsignacion';

@Injectable()
export class AsignacionMensajeroService {

    private formBuilder: FormBuilder;
    public form: FormGroup;

    private url = 'http://localhost:8081';

    constructor(private http: Http, fb: FormBuilder) {
        this.formBuilder = fb;
        this.buildForm();
    }

    buildForm(): void {
        this.form = this.formBuilder.group({

        })

    }

    getPendingTask() {
        return this.http.get('app/feature-areas/main/services/listaAsignaciones.json')
            .map(res => res.json().data);
    }

    getTask() {
        let data = this.entryData();
        console.log("DATA => "+data);
        let headers = this.getHeaders();
        return this.http.post(`${this.url}/query/executions`, data, { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    }

    private entryData() {
        let data = `
        {
            "processDefinitionId": "portalLogistico:1:13",
            "activityId": "asignacionRecogida"
        }
         `;
         return data;
    }

    private getHeaders() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Accept', 'application/json');
        headers.append('Authorization', 'Basic ' + btoa('admin:admin'));
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, x-auth-token, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        headers.append('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
        headers.append('Access-Control-Allow-Credentials', "true");
        headers.append('X-Requested-With', 'XMLHttpRequest');
        return headers;
    }

    getMessengersList() {
        let headers = this.getHeaders();
        return this.http.get(`${this.url}/identity/users`, { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        console.log("extractedData => ");
        console.log(body);
        return body.data || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}