import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { client } from '../../../model/clients';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { WorkRequest } from '../../../model/solicitud-recogida/work-request';

import { ActivitiRestApi } from '../../../model/commons/mckupApi';

@Injectable()
export class SolicitudRecogidaService {

    private clientUrl = 'app/feature-areas/main/services/ClientAddress.json'
    private apiUrl = 'http://localhost:8081/runtime/process-instances';
    
    clientes: client[];
    private formBuilder: FormBuilder;
    public form: FormGroup;
    public recogidaCliente: WorkRequest = {
        parentId: null,
        workType: '',
        status: 'Activo',
        priority: '',
        processingType: '',
        description: '',
        customer: {
            id: '',
            name: '',
            address: '',
            zone: '',
            city: ''
        },
        customerPatient: '',
        paymentDocuments: [],
        quantity: null,
        pickUpDate: '',
        flash: null,
        paymentRequired: null,
        courier: '',
        sender: '',
        clientName: '',
        branchOffice: '',
        guideNumber: '',
        sentDate: ''
    };

    constructor(private http: Http, fb: FormBuilder) {
        this.formBuilder = fb;
        this.buildForm();
    }

    buildForm(): void {
        this.form = this.formBuilder.group({
            name: [this.recogidaCliente.customer.name, Validators.required],
            address: [this.recogidaCliente.customer.address, Validators.required],
            zone: [this.recogidaCliente.customer.zone, Validators.required],
            quantity: [this.recogidaCliente.quantity, Validators.required],
            pickUpDate: [this.recogidaCliente.pickUpDate, Validators.required],
            flash: [this.recogidaCliente.flash],
            paymentRequired: [this.recogidaCliente.paymentRequired]
        })
        
    }

        getClients(): Observable<any[]> {
        return this.http.get(`${this.clientUrl}`)
            .map(this.extractData)
            .catch(this.handleError);
    }

    getCustomers() {
        return this.http.get(`${this.clientUrl}`)
            .toPromise()
            .then((res: Response) => res.json().data)
            .then(data => { return data; });
            //.then(res => <any[]>res.json().data)
    }



    createCollectRequest(wr: WorkRequest): Observable<Response> {
        let data = this.entryData(wr.customer.name, wr.customer.address, wr.customer.zone, wr.priority, wr.quantity, wr.flash, wr.pickUpDate);
        console.log("DATA => "+data);
        let headers = this.getHeaders();

        let resultado$ = this.http
            .post(`${this.apiUrl}`, data, { headers: headers })
            .map((res: Response) => res.json())
            .catch(this.handleError);
        return resultado$;
        //console.log("JSON savePost =>" + JSON.stringify(wr));
    }


    //Eliminar posteriormente
    private entryData(cname, caddress, zone, priority, quantity, flash, date) {
        let data = `
        {
            "processDefinitionId":"portalLogistico:1:13",
            "businessKey":"portalLogistico",
            "variables": [{
                "name":"es_recogida",
                "value":"true"
            },
            {
                "name":"customerName",
                "value":"`+cname+`"
            },
            {
                "name":"customerAddress",
                "value":"`+caddress+`"
            },
            {
                "name":"zone",
                "value":"`+zone+`"
            },
            {
                "name":"priority",
                "value":"`+priority+`"
            },
            {
                "name":"quantity",
                "value":"`+quantity+`"
            },
             {
                "name":"flash",
                "value":"`+flash+`"
            },
            {
                "name":"collectDate",
                "value":"`+date+`"
            }

            ]
        }`;
        return data;
    }

    private getHeaders() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Accept', 'application/json');
        headers.append('Authorization', 'Basic ' + btoa('admin:admin'));
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, x-auth-token, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        headers.append('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
        headers.append('Access-Control-Allow-Credentials', "true");
        headers.append('X-Requested-With', 'XMLHttpRequest');
        return headers;
    }

    private extractData(res: Response) {
        let body = res.json();
        //console.log("extractedData => " + JSON.stringify(body));
        return body.data || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}

function handleError(error: any) {
    let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
    console.error("ERROR =>"+errorMsg);
    return Observable.throw(errorMsg);
}

function mapPersons(response: Response): any[] {
    console.log("json2 => " + JSON.stringify(response.json()));
    return response.json().results.map();
}
