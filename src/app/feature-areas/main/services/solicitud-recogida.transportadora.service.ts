import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { client } from '../../../model/clients';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { WorkRequest } from '../../../model/solicitud-recogida/work-request';

@Injectable()
export class SolicitudRecogidaTransportadoraService {

    private apiUrl = 'http://localhost:8081/runtime/process-instances';

    private formBuilder: FormBuilder;
    public form: FormGroup;
    public recogidaTransportadora: WorkRequest = {
        parentId: null,
        workType: '',
        status: 'Activo',
        priority: '',
        processingType: '',
        description: '',
        customer: {
            id: '',
            name: '',
            address: '',
            zone: '',
            city: ''
        },
        customerPatient: '',
        paymentDocuments: [],
        quantity: null,
        pickUpDate: '',
        flash: null,
        paymentRequired: null,
        courier: '',
        sender: '',
        clientName: '',
        branchOffice: '',
        guideNumber: '',
        sentDate: '',
        estimatedDate: null
    };

    constructor(fb: FormBuilder, private http: Http) {
        this.formBuilder = fb;
        this.buildForm();
    }

    buildForm(): void {
        this.form = this.formBuilder.group({
            clientName: [this.recogidaTransportadora.clientName],
            sender: [this.recogidaTransportadora.sender, Validators.required],
            branchOffice: [this.recogidaTransportadora, Validators.required],
            courier: [this.recogidaTransportadora.courier, Validators.required],
            guideNumber: [this.recogidaTransportadora.guideNumber, Validators.required],
            sentDate: [this.recogidaTransportadora.sentDate, Validators.required]
        })
    }

    createCollectRequest(wr: WorkRequest): Observable<Response> {
        let data = this.entryData();
        //console.log("DATA => "+data);
        let headers = this.getHeaders();
        let resultado$ = this.http
            .post(`${this.apiUrl}`, data, { headers: headers })
            .map((res: Response) => res.json())
            .catch(this.handleError);
        return resultado$;
    }


    //Eliminar posteriormente
    private entryData() {
        let data = `
        {
            "processDefinitionId":"portalLogistico:1:13",
            "businessKey":"portalLogistico",
            "variables": [{
                "name":"es_recogida",
                "value":"true"
            }]
        }`;
        return data;
    }

    private getHeaders() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Accept', 'application/json');
        headers.append('Authorization', 'Basic ' + btoa('admin:admin'));
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, x-auth-token, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        headers.append('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
        headers.append('Access-Control-Allow-Credentials', "true");
        headers.append('X-Requested-With', 'XMLHttpRequest');
        return headers;
    }

    private extractData(res: Response) {
        let body = res.json();
        //console.log("extractedData => " + JSON.stringify(body));
        return body.data || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
