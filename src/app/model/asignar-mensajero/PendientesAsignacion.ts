export interface PendingModel {
    rdate: string;
    client: string;
    boffice?: string;
    courier?: string;
    messenger?: string;
    receivedDate?: string;
    sender?: string;
    address?: string;
}
