export interface MessengersModel {
    id: string;
    firstName?: string;
    lastName?: string;
    url?: string;
    email?: string;
}
