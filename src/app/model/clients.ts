import { address } from './address';

export interface client {
      id: number;
      name: string;
      address: address[];
      city: string;
}
