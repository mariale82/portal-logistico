import { ActivitiRestApiVariables } from './mckupApiVariables';

export interface ActivitiRestApi {
    processDefinitionId: string;
    businessKey: string;
    variables?: ActivitiRestApiVariables[];
}
