export interface PendingModel {
    rdate: string;
    client: string;
    boffice?: string;
    courier?: string;
    address?: string;
    sender?: string;
    receivedDate?: Date;
    collected?: boolean;
}

