export interface WorkRequest {
    id?: Number;
    parentId: String;
    status: String;
    workType: String;
    priority: String;
    processingType: String;
    description: String;
    courier: string;
    clientName: string;
    guideNumber: string;
    receivedDate: string;

}