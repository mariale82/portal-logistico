export interface ClientModel {
    id: string;
    name: string;
    address: string;
    zone: string;
    city?: string;
}
