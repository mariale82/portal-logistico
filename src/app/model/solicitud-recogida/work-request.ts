import { ClientModel } from './client';

export interface WorkRequest {
    id?: Number;
    parentId: String;
    status: String;
    workType: String;
    priority: String;
    processingType: String;
    description: String;
    customer: ClientModel;
    customerPatient: String;
    paymentDocuments: String[];
    quantity: number;
    pickUpDate: String;
    flash: boolean;
    paymentRequired: boolean;
    courier: string;
    sender: string;
    clientName: string;
    branchOffice: string;
    guideNumber: string;
    sentDate: string;
    estimatedDate: Date;

}