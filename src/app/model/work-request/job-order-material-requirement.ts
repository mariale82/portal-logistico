
import { Equipment } from '../equipment-model/equipment'
import { MaterialClass } from '../material-model/material-class'
import { MaterialDefinition } from '../material-model/material-definition'
import { MaterialLot } from '../material-model/material-lot'
import { MaterialSubLot } from '../material-model/material-sub-lot'
import { JobOrderMaterialRequirementProperty } from '../work-request/job-order-material-requirement-property'



export interface JobOrderMaterialRequirement {

    id: number;
    code: String;
    description: String;
    location: Equipment;
    hierarchyScope: String;
    materialClass: MaterialClass
    materialDefinition: MaterialDefinition;
    materialLot: MaterialLot;
    materialSubLot: MaterialSubLot;
    materialUse: String;
    storageLocation: Equipment;
    quantity: Number;
    unitOfMeasure: String;
    assemblyRequirement: JobOrderMaterialRequirement[];
    assemblyType: String;
    assemblyRelationShip: String;
    properties: JobOrderMaterialRequirementProperty[];
}