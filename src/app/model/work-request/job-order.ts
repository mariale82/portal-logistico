
import { Equipment } from '../equipment-model/equipment'
import { Parameter } from '../commons/parameter'
import { JobOrderMaterialRequirement } from './job-order-material-requirement'


export interface JobOrder {
    tenantId: Number;
    id?: Number;
    code?: String;
    description?: String;
    location?: Equipment;
    hierarchyScope?: String;
    workType?: String;
    workMaster: String;
    workMasterId?: Number;
    workMasterVersion?: String;
    startTime?: Date;
    endTime?: Date;
    priority?: String;
    command?: String;
    commandRule?: String;
    dispatchStatus?: String;
    parameters: Parameter[];
    materialRequirements: JobOrderMaterialRequirement[];
}