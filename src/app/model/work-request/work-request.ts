
import { Equipment } from '../equipment-model/equipment'
import { JobOrder } from './job-order'

export interface WorkRequest {
    tenantId: Number;
    id?: Number;
    code?: String;
    description?: String;
    location?: Equipment;
    hierarchyScope?: String;
    workType?: String;
    startTime?: Date;
    endTime?: Date;
    priority?: String;
    jobOrders: JobOrder[];
    workRequests: WorkRequest[];
}