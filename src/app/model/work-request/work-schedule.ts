
import { Equipment } from '../equipment-model/equipment'
import { WorkRequest } from './work-request'

export interface WorkSchedule {
    tenantId: Number;
    id: Number;
    code: String;
    description: String;
    location: Equipment;
    hierarchyScope: String
    workType: String;
    startTime: Date;
    endTime: Date;
    scheduleState: String
    publishedDate: Date;
    workSchedules: WorkSchedule[];
    workRequests: WorkRequest[];
}