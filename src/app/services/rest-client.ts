import { Injectable, Injector } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Resource } from 'ng2-resource-rest';

@Injectable()
export class RestClient extends Resource {
  constructor(http: Http, injector: Injector) {
    super(http, injector);
  }

  getHeaders(methodOptions?: any): any {
    let headers = super.getHeaders();
    Object.assign(headers, {
      'Authorization': 'Basic ' + btoa('admin:admin')
    });
    return headers;
  }

  getUrl(methodOptions?: any): string | Promise<string> {
    let resPath = super.getUrl();
    return 'http://ibisa-manufacturing-dev.eastus2.cloudapp.azure.com:8081' + resPath;
  }

  resolveHAL(URL) {
    let headers = new Headers();
    headers.append('Authorization', 'Basic ' + btoa('admin:admin'));
    return this.http.get(URL, new RequestOptions({headers: headers}))
      .map((data) => data.json() || {});
  }
}
